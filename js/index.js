"use strict";
const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const root = document.getElementById("root");

for (let key in books) {
  let temp = books[key];
  let list = document.createElement("ul");
  try {
    for (let innerKey in books[key]) {
      if (!temp.author) {
        throw new Error("Author");
      }
      if (!temp.name) {
        throw new Error("Name");
      }
      if (!temp.price) {
        throw new Error("Price");
      }
      let listItem = document.createElement("li");
      listItem.append(`${innerKey}: ${books[key][innerKey]}`);
      list.append(listItem);
      root.append(list);
    }
  } catch (e) {
    console.log(`Fatal. Property ${e.message} is not found`);
  }
}
